# PlaylistManager
A Qt framework program made to perform operations with .m3u;.m3u8 (and possibly other formats) playlist files.
Current features:
1. Check missing songs from the filesystem in your playlist.
2. Change directory of entire playlist.

Both features work on multiple files at once.

Build commands:
1. qmake PlaylistManager.pro
2. make -j8

Easier way to build is to download Qt Creator and build it there.
