#ifndef STRINGREPDRIVER_H
#define STRINGREPDRIVER_H
#include "path.h"
#include <vector>
#include <iostream>
#include <fstream>
using std::vector;
class StringRepDriver:public Path
{
    private:
        std::vector <string> what, with, ex;
        std::vector <int> exIndex;
        int replace(string repWhat, string repWith, string inputName, string outputName, int exCount, int currentEx);
        bool findString(string str, string line);
        std::string replaceAllInstances(std::string text, std::string what, std::string to);
    public:
        StringRepDriver();
        StringRepDriver(vector <string> _what,vector <string> _to,
                        vector <int> _exIndex,vector <string> _ex, string output)
                        :what(_what),with(_to),ex(_ex),exIndex(_exIndex)
        {
            setOutput(output);
        }
        StringRepDriver(std::vector <string> _what,std::vector <string> _to)
            :what(_what),with(_to){}
        ~StringRepDriver(){};
        int replaceLoop(int count,int exCount,std::vector <string> files);
        void setFileInAndOut(std::string file,int count,int i);
};
#endif // STRINGREPDRIVER_H
