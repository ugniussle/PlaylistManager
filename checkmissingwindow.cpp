#include "checkmissingwindow.h"
#include "ui_checkmissingwindow.h"
#include "stringrepdriver.h"
#include <path.h>
#include <QFile>
#include <QTextStream>
#include <QDebug>
//#include <QStringConverter>

CheckMissingWindow::CheckMissingWindow(QString file,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CheckMissingWindow)
{
    ui->setupUi(this);
    //load stylesheet from resources
    QFile stylesheet(":styles/mainStyle.qss");
    stylesheet.open(QFile::ReadOnly);
    QString style(stylesheet.readAll());
    this->setStyleSheet(style);
    checkMissingSongs(file);
    Path path;
    QString title=path.filename(file);
    setWindowTitle(title);

}

CheckMissingWindow::~CheckMissingWindow()
{
    delete ui;
}
void CheckMissingWindow::checkMissingSongs(QString file){
    QFile playlistFile(file);
    playlistFile.open(QFile::ReadOnly);
    QTextStream playlistStream(&playlistFile);
    //playlistStream.setEncoding(QStringConverter::Utf8);
    while(!playlistStream.atEnd()){
        QString line=playlistStream.readLine();
        if(line.size()>0){
            QFile song(line);
            if(song.exists()==false&&line[0]!='#') ui->missingList->addItem(line);
        }
    }
}
