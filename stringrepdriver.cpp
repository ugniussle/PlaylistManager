#include "stringrepdriver.h"

int StringRepDriver::replaceLoop(int count, int exCount, std::vector<string> files){
    for (int i = 0; i < count; i++){
        string repWhat=what[i], repWith=with[i];

        for (int j = 0; j < (int)files.size(); j++){
            setFileInAndOut(files[j], count, i);

            std::cout << "input " << getFileIn() << " output " << getFileOut() << '\n';

            int sum = replace(repWhat, repWith, getFileIn(), getFileOut(), exCount, i);
            sumTotal(sum);

            //TEMP FILE REMOVAL
            if(i != 0) QFile::remove(QString::fromStdString(getFileIn()));
        }
    }

    return getTotalChanges();
}

int StringRepDriver::replace(string repWhat, string repWith, string inputName, string outputName, int exCount, int currentEx){
    std::ifstream text(inputName.c_str());
    std::ofstream output(outputName.c_str());

    int count=0;

    while(!text.eof() && text.is_open()){
        bool exCheck = false;

        string str;
        std::getline(text, str);

        for(int i = 0; i < exCount; i++){
            if (findString(ex[i], str) && ((exIndex[i] - 1) == currentEx))
                exCheck = true;
        }

        if (exCheck == false)
            output << replaceAllInstances(str, repWhat, repWith) << std::endl;
        else
            output << str;
    }

    text.close();
    output.close();

    return count;
}

bool StringRepDriver::findString(string str, string line){
    if(str == "") return false;

    if(line.find(str) == std::string::npos) return false;  //string::find returns npos when string is not found
    else return true;
}

std::string StringRepDriver::replaceAllInstances(std::string text,std::string what,std::string to){
    size_t wLen = what.length(), pos = 0;

    while(true){
        pos = text.find(what, pos);

        if(pos == std::string::npos) break;

        text.replace(pos, wLen, to);

        pos += to.length();
    }
    return text;
}

void StringRepDriver::setFileInAndOut(std::string file,int count,int i){ //i - loop position, count - number of changes
    string Filename = filename(file);

    if ((i == (count - 1)) && (i != 0)){        //NOT FIRST BUT LAST RUN
        setFileIn(getOutput() + (char)(i + 48) + Filename);
        setFileOut(getOutput() + Filename);
    }
    else if ((count==1) && (i == 0)){           //FIRST AND LAST RUN
        setFileIn(file);
        setFileOut(getOutput() + Filename);
    }
    else if ((i != (count - 1)) && (i == 0)){   //FIRST RUN BUT NOT LAST
        setFileIn(file);
        setFileOut(getOutput() + (char)(i + 49) + Filename);
    }
    else{                                       //NOT FIRST AND NOT LAST
        setFileIn(getOutput() + (char)(i + 48) + Filename);
        setFileOut(getOutput() + (char)(i + 49) + Filename);
    }
}
