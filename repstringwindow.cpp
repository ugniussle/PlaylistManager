#include "repstringwindow.h"
#include "ui_repstringwindow.h"
#include "stringrepdriver.h"
#include <path.h>
#include <QFile>
#include <iostream>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
RepStringWindow::RepStringWindow(std::vector <std::string> _files,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RepStringWindow),
    files(_files)
{
    ui->setupUi(this);

    QFile stylesheet(":styles/mainStyle.qss");
    stylesheet.open(QFile::ReadOnly);
    QString style(stylesheet.readAll());
    this->setStyleSheet(style);

    ui->changeTable->setColumnCount(2);
    ui->exceptionTable->setColumnCount(2);

    Path path;
    QString title;

    for(int i=0;i<(int)files.size();i++)
        title+=path.filename(QString::fromStdString(path.filename(files[i])))+' ';

    setWindowTitle(title);
}

RepStringWindow::~RepStringWindow(){
    delete ui;
}

void RepStringWindow::on_addChange_clicked(){
    QString what=ui->changeWhat->text();
    QString to=ui->changeTo->text();

    if(what==""||to=="")return;

    int row=ui->changeTable->rowCount();
    ui->changeTable->setRowCount(row+1);

    QTableWidgetItem *item[2];

    for(int i=0;i<2;i++)item[i]=new QTableWidgetItem;

    item[0]->setText(what);item[1]->setText(to);

    for(int i=0;i<2;i++)ui->changeTable->setItem(row, i, item[i]);

    ui->changeWhat->setText("");  //reset fields
    ui->changeTo->setText("");
}


void RepStringWindow::on_changeRemove_clicked()
{

}

void RepStringWindow::on_convertButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Set Output Directory"),
                                                    "/home",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    if(dir=="")return;

    std::vector <string> what,to;
    tableToVectors(ui->changeTable, what, to);

    for(int i = 0; i < (int)what.size(); i++){
        std::cout << i << ' ' << what[i] << ' ' << to[i] << std::endl;
    }

    std::vector <string> _exNum, ex;
    std::vector <int> exNum;

    for(int i = 0; i < (int)_exNum.size(); i++){
        exNum.push_back(std::stoi(_exNum[i]));
    }

    tableToVectors(ui->exceptionTable, _exNum, ex);

    for(int i = 0; i < (int)ex.size(); i++){
        std::cout << i << ' ' << _exNum[i] << ' ' << ex[i] << std::endl;
    }

    StringRepDriver driver(what, to, exNum, ex, dir.toStdString());
    driver.replaceLoop(what.size(), exNum.size(), files);
}

void RepStringWindow::tableToVectors(QTableWidget *table, std::vector <std::string> &what, std::vector <std::string> &to){
    for(int i=0; i<table->rowCount(); i++){
        what.push_back(table->item(i,0)->text().toStdString());
        to.push_back(table->item(i,1)->text().toStdString());
    }
}

void RepStringWindow::on_addException_clicked()
{
    QString exNum=ui->exceptionNum->text();  //fields
    QString ex=ui->exceptionText->text();

    if(exNum==""||ex=="")return;

    int row=ui->exceptionTable->rowCount();

    ui->exceptionTable->setRowCount(row+1);

    QTableWidgetItem *item[2];

    for(int i=0;i<2;i++)item[i]=new QTableWidgetItem;

    item[0]->setText(exNum);item[1]->setText(ex);

    for(int i=0;i<2;i++)ui->exceptionTable->setItem(row, i, item[i]);

    ui->exceptionNum->setText("");  //reset fields
    ui->exceptionText->setText("");
    //delete(item[0]);delete(item[1]);
}

void RepStringWindow::on_loadTemplate_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open Template"), "/home");

    if(filename=="") return;

    std::vector <string> what,to;

    bool check=templateToVectors(filename,what,to);

    if(check){
        QMessageBox msg;
        msg.setText("Bad template file.");
        msg.exec();
        return;
    }

    vectorsToTable(ui->changeWhat, ui->changeTo, what, to);
}

void RepStringWindow::on_saveTemplate_clicked()
{
    std::vector <string> what,to;

    tableToVectors(ui->changeTable,what,to);

    QString filename = QFileDialog::getSaveFileName(this,tr("Save Template"),"/home");

    std::cout<<filename.toStdString()<<" what size "<<what.size()<<std::endl;

    if(filename=="")return;

    std::ofstream file(filename.toStdString().c_str());

    for(int i=0;i<(int)what.size();i++){
        file << what[i] << std::endl
             << to[i]   << std::endl;
    }

}

void RepStringWindow::vectorsToTable(QLineEdit *line1, QLineEdit *line2, std::vector <std::string> &what, std::vector <std::string> &to){
    for(int i = 0; i < (int)what.size(); i++){
        line1->setText(QString::fromStdString(what[i]));
        line2->setText(QString::fromStdString(to[i]));

        on_addChange_clicked();
    }
}

bool RepStringWindow::templateToVectors(QString filename, std::vector <std::string> &what, std::vector <std::string> &to){
    int lineCount=0;

    std::ifstream f(filename.toStdString().c_str());

    while(!f.eof()){
        string str;
        std::getline(f, str);

        if(str != "") lineCount++;
    }

    f.close();

    std::ifstream file(filename.toStdString().c_str());

    for(int i = 0; i < (lineCount / 2); i++){
        std::string tmp;

        std::getline(file, tmp);
        what.push_back(tmp);

        std::getline(file, tmp);
        to.push_back(tmp);
    }

    if(lineCount % 2 != 0) return 1;

    return 0;
}

