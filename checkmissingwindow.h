#ifndef CHECKMISSINGWINDOW_H
#define CHECKMISSINGWINDOW_H

#include <QWidget>

namespace Ui {
class CheckMissingWindow;
}

class CheckMissingWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CheckMissingWindow(QString file,QWidget *parent = nullptr);
    ~CheckMissingWindow();

private:
    Ui::CheckMissingWindow *ui;
    void checkMissingSongs(QString file);
};

#endif // CHECKMISSINGWINDOW_H
