#ifndef REPSTRINGWINDOW_H
#define REPSTRINGWINDOW_H

#include <QWidget>
#include <QTableWidget>
namespace Ui {
class RepStringWindow;
}

class RepStringWindow : public QWidget
{
    Q_OBJECT

public:
    static bool templateToVectors(QString filename, std::vector <std::string> &what, std::vector <std::string> &to);
    explicit RepStringWindow(std::vector <std::string> _files, QWidget *parent = nullptr);
    ~RepStringWindow();

private slots:
    void on_addChange_clicked();

    void on_changeRemove_clicked();

    void on_convertButton_clicked();

    void on_addException_clicked();

    void on_loadTemplate_clicked();

    void on_saveTemplate_clicked();


private:
    Ui::RepStringWindow *ui;
    std::vector <std::string> files;
    void tableToVectors(QTableWidget *table,std::vector <std::string> &a,std::vector <std::string> &b);
    void vectorsToTable(QLineEdit *line1,QLineEdit *line2,std::vector <std::string> &a,std::vector <std::string> &b);

};

#endif // REPSTRINGWINDOW_H
