QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG += console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    changes.cpp \
    checkmissingwindow.cpp \
    main.cpp \
    mainwindow.cpp \
    path.cpp \
    repstringwindow.cpp \
    stringrepdriver.cpp \
    todynamicwindow.cpp

HEADERS += \
    changes.h \
    checkmissingwindow.h \
    mainwindow.h \
    path.h \
    repstringwindow.h \
    stringrepdriver.h \
    todynamicwindow.h

FORMS += \
    checkmissingwindow.ui \
    mainwindow.ui \
    repstringwindow.ui \
    todynamicwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    styles/mainStyle.qss

RESOURCES += \
    resources.qrc
