#ifndef CHANGES_H
#define CHANGES_H


class Changes
{
    private:
        static int totalChanges;
    public:
        Changes();
        static int getTotalChanges(){
            return totalChanges;
        }
        void sumTotal(int add){
            totalChanges += add;
        }
};

#endif // CHANGES_H
