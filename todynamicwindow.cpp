#include "todynamicwindow.h"
#include "ui_todynamicwindow.h"
#include <QFile>
#include "path.h"
ToDynamicWindow::ToDynamicWindow(std::vector <std::string> _files,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ToDynamicWindow),
    files(_files)
{
    ui->setupUi(this);
    QFile stylesheet(":styles/mainStyle.qss");
    stylesheet.open(QFile::ReadOnly);
    QString style(stylesheet.readAll());
    this->setStyleSheet(style);
    Path path;
    QString title;
    for(int i=0;i<(int)files.size();i++)
    title+=path.filename(QString::fromStdString(path.filename(files[i])))+' ';
    setWindowTitle(title);
}

ToDynamicWindow::~ToDynamicWindow()
{
    delete ui;
}
