#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_checkMissing_clicked();

    void on_actionLoad_Playlist_triggered();

    void on_clearPlaylists_clicked();

    void on_changeDir_clicked();

    std::vector <std::string>listToVector(QList <QListWidgetItem*> list);

    void on_toDynamic_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
