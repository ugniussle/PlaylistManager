#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "checkmissingwindow.h"
#include "stringrepdriver.h"
#include "todynamicwindow.h"
#include <repstringwindow.h>
#include <QFile>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QDebug>
#include "path.cpp"
#include <iostream>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //load stylesheet from resources
    QFile stylesheet(":styles/mainStyle.qss");
    stylesheet.open(QFile::ReadOnly);
    QString style(stylesheet.readAll());
    this->setStyleSheet(style);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_checkMissing_clicked()
{
    QList <QListWidgetItem*> list;
    list=ui->playlistList->selectedItems();
    std::vector <std::string> files=listToVector(list);
    if(files.size()==0) return;
    for(int i=0;i<(int)files.size();i++){
        CheckMissingWindow *checkMissingWindow = new CheckMissingWindow(QString::fromStdString(files[i]),nullptr);
        checkMissingWindow->setAttribute(Qt::WA_DeleteOnClose);
        checkMissingWindow->show();
    }
}

void MainWindow::on_actionLoad_Playlist_triggered()
{
    QFileDialog *dialog = new QFileDialog;
    dialog->setFileMode(QFileDialog::ExistingFiles);
    dialog->show();
    QStringList filenames;
    if(dialog->exec()) filenames = dialog->selectedFiles();
    if(filenames.length()==0) return;
    else if(filenames.length()>0) ui->playlistList->addItems(filenames);
}


void MainWindow::on_clearPlaylists_clicked()
{
    ui->playlistList->clear();
}

void MainWindow::on_changeDir_clicked()
{
    if(ui->playlistList->selectedItems().size()==0) return;
    QList <QListWidgetItem*> list;
    list=ui->playlistList->selectedItems();
    std::vector <std::string> stringList=listToVector(list);
    RepStringWindow *repStringWindow = new RepStringWindow(stringList,nullptr);
    repStringWindow->setAttribute(Qt::WA_DeleteOnClose);
    repStringWindow->show();
}
std::vector <std::string> MainWindow::listToVector(QList <QListWidgetItem*> list){
    std::vector <std::string> vec;
    for(int i=0;i<list.size();i++){
        vec.push_back(list.value(i)->text().toStdString());
    }
    return vec;
}


void MainWindow::on_toDynamic_clicked()
{
    if(ui->playlistList->selectedItems().size()==0) return;
    QList <QListWidgetItem*> list;
    list=ui->playlistList->selectedItems();
    std::vector <std::string> stringList=listToVector(list);
    ToDynamicWindow *toDynamicWindow = new ToDynamicWindow(stringList,nullptr);
    toDynamicWindow->setAttribute(Qt::WA_DeleteOnClose);
    toDynamicWindow->show();
}

