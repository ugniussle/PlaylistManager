#ifndef TODYNAMICWINDOW_H
#define TODYNAMICWINDOW_H

#include <QDialog>

namespace Ui {
class ToDynamicWindow;
}

class ToDynamicWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ToDynamicWindow(std::vector <std::string> _files, QWidget *parent = nullptr);
    ~ToDynamicWindow();

private:
    Ui::ToDynamicWindow *ui;
    std::vector <std::string> files;
};

#endif // TODYNAMICWINDOW_H
