#include "mainwindow.h"
#include "stringrepdriver.h"
#include <QApplication>
#include <fstream>
#include <cmath>
#include <string>
#include "repstringwindow.h"
#include <QDirIterator>

#ifdef Q_OS_WIN
#include <Windows.h>
#endif

int main(int argc, char *argv[]) {
    if (argc > 1) {
        std::vector <std::string> files;
        std::string dir = "", tmplate = "";

        bool fd = false;
        QString fDir = "";

        for(int i = 1; i + 1 < argc; i = i + 2) {
            if(strcmp(argv[i], "-f") == 0) {
                files.push_back(argv[i + 1]);
            }
            else if(strcmp(argv[i], "-d") == 0) {
                dir = argv[i + 1];
            }

            else if(strcmp(argv[i], "-t") == 0) {
                tmplate = argv[i + 1];
            }
            else if(strcmp(argv[i], "-fd") == 0) {
                fd = true;
                fDir = argv[i + 1];
            }
            else {
                std::cout << argv[i] << std::endl << argv[i + 1] << std::endl;
                std::cout << "-f to specify playlist files (one at a time), -fd for playlist file directory, -d to set the output directory, -t to point to the template file" << std::endl;
                std::cout << "example 1:" << std::endl;
                std::cout << "PlaylistManager -f \"path\\to\\playlist.m3u8\" -d \"path\\to\\outputDir\" -t \"path\\to\\template.txt\"" << std::endl;
                std::cout << "example 2:" << std::endl;
                std::cout << "PlaylistManager -fd \"path\\to\\playlistDir\" -d \"path\\to\\outputDir\" -t \"path\\to\\template.txt\"" << std::endl;

                return 1;
            }
        }

        if(files.size() == 0 && fd == false) {
            std::cout << "no files given, use -f to add files" << std::endl;
            return 1;
        }
        else if(tmplate == "") {
            std::cout << "no template given, use -t to add a template" << std::endl;
            return 1;
        }
        else if(dir == "") {
            std::cout << "no directory given, use -d specify the output directory" << std::endl;
            return 1;
        }

        if(fd == true) {
            QDirIterator it(fDir, QDir::Filter::Files);

            while(it.hasNext()) {
                try {
                    QFile f(it.next());

                    std::string fn = f.fileName().toStdString();
                    std::string ext = fn.substr(fn.find_last_of(".") + 1);

                    if(ext == "m3u" || ext == "m3u8")
                        files.push_back(fn);
                } catch(std::exception& e) {
                    std::cerr << e.what() << std::endl;
                }
            }
        }

        std::vector <std::string> what, to;

        RepStringWindow::templateToVectors(QString::fromStdString(tmplate), what, to);

        StringRepDriver driver(what, to);
        driver.setOutput(dir);

        driver.replaceLoop(what.size(), 0, files);

        return 0;
    }
    else {
#ifdef Q_OS_WIN
        ::ShowWindow(::GetConsoleWindow(), SW_HIDE);
#endif
        QApplication a(argc, argv);
        MainWindow w;
        w.show();

        return a.exec();
    }
}
