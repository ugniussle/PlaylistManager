#ifndef PATH_H
#define PATH_H

#include <string>
#include <QString>
#include <QDir>
#include <changes.h>
using std::string;
class Path:public Changes
{
    private:
        string 	input,
                output,
                current,
                fileIn,
                fileOut;
        char dirSlash;
    public:
        Path(){
            dirSlash=OPCheck(QDir::currentPath().toStdString());
        }
        QString filename(QString str){
            return QString::fromStdString(filename(str.toStdString()));
        }
        string filename(string str){
            string c="";
            int length = str.length();
            for(int i=0;i<length;i++){
                c+=str[i];
                char tmp=str[i];
                if(tmp=='/'||tmp=='\\'){
                    c="";
                }
            }
            return c;
        }
        string pathFromFile(string str){
            string fileOnly=filename(str);
            return str.replace(str.find(fileOnly),str.length(),"");
        }
        void setInput(string str){input=str;}
        void setOutput(string str){output=str;}
        void setFileIn(string str){fileIn=str;}
        void setFileOut(string str){fileOut=str;}
        char OPCheck(string path){
            if(path[0]=='/'){
                return '/';
            }
            else{
                return '\\';
            }
        }
        string getInput(){return input+dirSlash;}
        string getOutput(){return output+dirSlash;}
        string getFileIn(){return fileIn;}
        string getFileOut(){return fileOut;}
        char getDir(){return dirSlash;}
};

#endif // PATH_H
